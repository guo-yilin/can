# CAN

#### 介绍

CAN utils for OpenHarmony


#### 目录结构

```
.
├── BUILD.gn      # OpenHarmony BUILD.gn
├── canutils      # canutils 源码
├── libsocketcan  # libsocketcan 源码
├── prebuilts     # 预编译好的can相关工具，适用OpenHarmony ARM 32位系统
├── README.en.md
└── README.md
```

#### 工具简介

canutils 工具包内含 5 个独立的程序：canconfig、candump、canecho、cansend、cansequence。程序的功能简述如下：

| 程序名         | 功能                                        |
| ----------- | ----------------------------------------- |
| canconfig   | 用于配置CAN总线接口的参数，主要是波特率和模式                  |
| candump     | 从CAN总线接口接收数据并以十六进制形式打印到标准输出，也可以输出到指定文件    |
| canecho     | 把从CAN总线接口接收到的所有数据重新发送到CAN总线接口             |
| cansend     | 往指定的CAN总线接口发送指定的数据                        |
| cansequence | 往指定的CAN总线接口自动重复递增数字，也可以指定接收模式并校验检查接收的递增数字 |

> 工具常见用法：
> candump canX                                 // 接收can总线发来数据；
> ifconfig canX down                           // 关闭can设备，以便配置;
> ip link set canX up type can bitrate 250000  // 设置can波特率 (需配合ip工具)
> conconfig canX bitrate + 波特率
> canconfig canX start                         // 启动can设备；
> canconfig canX ctrlmode loopback on          // 回环测试；
> canconfig canX restart                       // 重启can设备；
> canconfig canX stop                          // 停止can设备；
> canecho canX                                 // 查看can设备总线状态；
> cansend canX --identifier=ID+数据            // 发送数据；
> candump canX --filter=ID:mask                // 使用滤波器接收ID匹配的数据


#### 交叉编译

# libsocketcan

```shell
CC=/home/umspark/openharmony/master/prebuilts/clang/ohos/linux-x86_64/llvm/bin/clang CXX=/home/umspark/openharmony/master/prebuilts/clang/ohos/linux-x86_64/llvm/bin/clang++ CFLAGS='-g -O2 --target=arm-linux-ohosmusl -march=armv7-a --sysroot=/home/umspark/openharmony/master/out/unionpi_tiger/obj/third_party/musl -D__MUSL__ -fPIC' LD=/home/umspark/openharmony/master/prebuilts/clang/ohos/linux-x86_64/llvm/bin/lld LDFLAGS='-v -fuse-ld=lld -fPIC' AR=/home/umspark/openharmony/master/prebuilts/clang/ohos/linux-x86_64/llvm/bin/llvm-ar  ./configure --host=arm-linux --prefix=/home/umspark/can/prebuilts  --disable-shared
```

# canutils

```shell 
export PKG_CONFIG_PATH=/home/umspark/tools/network/canutils/install/lib/pkgconfig
CC=/home/umspark/openharmony/master/prebuilts/clang/ohos/linux-x86_64/llvm/bin/clang CXX=/home/umspark/openharmony/master/prebuilts/clang/ohos/linux-x86_64/llvm/bin/clang++ CFLAGS='-g -O2 --target=arm-linux-ohosmusl -march=armv7-a --sysroot=/home/umspark/openharmony/master/out/unionpi_tiger/obj/third_party/musl -D__MUSL__ -fPIC -L/home/umspark/can/prebuilts/lib -lsocketcan' LD=/home/umspark/openharmony/master/prebuilts/clang/ohos/linux-x86_64/llvm/bin/lld LDFLAGS='-v -fuse-ld=lld -fPIC' AR=/home/umspark/openharmony/master/prebuilts/clang/ohos/linux-x86_64/llvm/bin/llvm-ar  ./configure --host=arm-linux --prefix=/home/umspark/can/prebuilts  --disable-shared
```

> 说明：编译链和--prefix路径改成自己实际路径

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
